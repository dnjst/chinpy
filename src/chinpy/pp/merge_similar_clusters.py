import numpy as np
import pandas as pd
import scanpy as sc
import math
from anndata import AnnData
from typing import Optional, Union

def merge_similar_clusters(adata : AnnData,
                           group_key : str,
                           layer : Optional[str] = None,
                           key_added : str = None,
                           gene_symbols : Optional[list] = None,
                           corr_cutoff : float = 0.50,
                           num_genes : int = 20,
                           fold_diff : float = 2,
                           test_method : str = 'wilcoxon',
                           padj : float = 0.05,
                           rename_by_size : bool = True,
                           return_cormatrix: bool = False) -> pd.DataFrame:
    """\
    Iteratively merge single-cell clusters based on differential gene expression, until they converge on a smaller number of clusters. Based upon [Musser et al., 2021.](https://git.embl.de/musser/profiling-cellular-diversity-in-sponges-informs-animal-cell-type-and-nervous-system-evolution/)

    Parameters
    ----------
    adata
        Annotated data matrix.
    group_key
        The column in `adata.obs` to summarize to determine groups, such as cluster names/numbers.
    layer
        The layer from which to draw expression values. None draws from `adata.X`.
    key_added
        The column in `adata.obs` to store the new merged cluster labels. Defaults to `group_key` + `_merged`.
    gene_symbols
        Which genes to include in the calculations. None uses all genes.
    corr_cutoff
        Only consider clusters with a correlation of less than this value.
    num_genes
        Minimum number of differentially expressed genes to look for.
    fold_diff
        Minimum fold-change of differential expression to look for.
    test_method
        Method parameter to pass to scanpy.tl.rank_genes_groups().
    padj
        Adjusted p-value threshold to use in calculating differential expression.
    rename_by_size
        Currently unused.
    return_cormatrix
        Rather than renaming in-place, will return the correlation matrix.

    Returns
    -------
    pd.DataFrame
        A matrix of cell cluster-wise gene expression levels.
    """
    if key_added == None:
        key_added = group_key + "_merged"
    # set up new annotated frame
    adata.obs[key_added] = adata.obs[group_key]
    if layer is not None:
        getX = adata[layer]
    else:
        getX = adata.X
    counter = 1
    while counter > 0:
        # average expression
        avg_exp = chinpy.pp.grouped_obs_mean(adata, group_key = key_added, layer=layer, gene_symbols=gene_symbols)
        # calculate pairwise correlations
        correlations = avg_exp.corr("pearson")
        # lower.tri to zero all of the redundant ones
        correlations = pd.DataFrame(np.tril(correlations), index=correlations.index, columns=correlations.columns)
        # make long
        correlations = pd.melt(correlations, ignore_index=False, var_name="cluster_2")
        correlations.index.name = "cluster_1"
        correlations = correlations.set_index('cluster_2',append=True)
        # sort by correlation descending
        correlations = correlations[correlations['value'] != 1].sort_values(by = "value", ascending=False)
        # filter based on cutoff
        correlations = correlations[correlations['value'] > corr_cutoff]
        #return(correlations)
        
        if correlations.shape[0] == 0:
            counter = 0 # end while loop
        else:
            print(str(avg_exp.shape[1]) + " Cell Types: filtered correlations matrix is of size " + str(correlations.shape[0]))
            passed = 0
            for thispair in correlations.index:
                cluster_1 = str(thispair[0])
                cluster_2 = str(thispair[1])
                print("Calculating sc.tl.rank_genes_groups between {0} and {1}...".format(cluster_1, cluster_2))
                sc.tl.rank_genes_groups(adata, key_added, groups=[cluster_1], reference=cluster_2, method=test_method, key_added="temp")
                # get list of de genes
                de_genes = pd.DataFrame({key: [i[0] for i in adata.uns['temp'][key]] for key in ['names', 'pvals_adj', 'logfoldchanges']})
                # pval filter
                de_genes = de_genes[de_genes['pvals_adj'] < padj]
                # fold change filter
                diff_markers_filtered = de_genes[de_genes['logfoldchanges'] > math.log(fold_diff, 2)]
                diff_markers_filtered2 = de_genes[de_genes['logfoldchanges'] < -math.log(fold_diff, 2)]
                print("...done! Found {0} upregulated and {1} downregulated DEG!".format(str(diff_markers_filtered.shape[0]), str(diff_markers_filtered2.shape[0])))
                if diff_markers_filtered.shape[0] < num_genes and diff_markers_filtered2.shape[0] < num_genes:
                    print("Merging pair " + cluster_1 + "::" + cluster_2 + "...")
                    new_clust = cluster_1 + "." + cluster_2
                    adata.obs[key_added].replace(cluster_1, new_clust, inplace=True)
                    adata.obs[key_added].replace(cluster_2, new_clust, inplace=True)
                    counter = 1
                    break
                else:
                    passed += 1
            if passed == correlations.index.shape[0]:
                counter = 0
    if return_cormatrix == True:
        return(correlations)
