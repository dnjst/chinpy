# grouped_obs_mean function modified from Isaac Virshup @ https://github.com/theislab/scanpy/issues/181
# Call as grouped_obs_mean(adata[:, marker_genes], ...)

import numpy as np
import pandas as pd
from anndata import AnnData
from typing import Optional, Union

def grouped_obs_mean(adata : AnnData,
    group_key : str,
    layer : Optional[str] = None,
    use_raw : bool = False,
    gene_symbols : Optional[list] = None,
    binary : bool = False,
    binary_cutoff : float = 0) -> pd.DataFrame:
    """\
    Summarize an AnnData object as a matrix of cluster-wise expression values. Expanded from a function by [Isaac Virshup](https://github.com/theislab/scanpy/issues/181).

    Parameters
    ----------
    adata
        Annotated data matrix.
    group_key
        The column in `adata.obs` to summarize to determine groups, such as cluster names/numbers.
    layer
        The layer from which to draw expression values. None draws from `adata.X`.
    use_raw
        Whether to draw expression values from `adata.raw.X`.
    gene_symbols
        Which genes to include in the calculations.
    binary
        If True, convert expression values into off/on calls based upon the binary_cutoff threshold.
    binary_cutoff
        A minimum expression level for a gene to be called "on" in binary mode.

    Returns
    -------
    pd.DataFrame
        A matrix of cell cluster-wise gene expression levels.
    """
    if layer is not None:
        getX = lambda x: x.layers[layer]
    elif use_raw is True:
        getX = lambda x: x.raw.X
    else:
        getX = lambda x: x.X
    if gene_symbols is not None:
        new_idx = adata.var[gene_symbols]
    else:
        new_idx = adata.var_names

    grouped = adata.obs.groupby(group_key)
    out = pd.DataFrame(
        np.zeros((adata.shape[1], len(grouped)), dtype=np.float64),
        columns=list(grouped.groups.keys()),
        index=new_idx
    )

    for group, idx in grouped.indices.items():
        X = getX(adata[idx])
        if binary == True:
            out[group] = np.ravel((X > binary_cutoff).mean(axis=0, dtype=np.float64))
        else:
            out[group] = np.ravel(X.mean(axis=0, dtype=np.float64))
    return out
