from pybiomart import Server
import pandas as pd
from typing import Optional, Union

def remap(dataset :str,
    longnames : bool = True,
    only11 : bool = False,
    onlyhs : bool = False,
    host : str = 'http://www.ensembl.org',
    return_dict : bool = True,
    cutnonnamed : bool = False) -> dict:
    """\
    Generate a mapping of gene IDs to names according to human homology rather than species-specific names, and in a non-ambiguous format.

    Parameters
    ----------
    dataset
        The species-specific dataset to be called from pybiomart.
    longnames
        Whether or not to use the GENE_NAME (GENE_STABLE_ID) format.
    only11
        Whether to set GENE_NAME to the human ortholog only in cases of 1:1 orthologs, or whether many:1 relationships are also named in this way. Including many:1 requires longnames=True to avoid duplicates.
    onlyhs
        If False, any genes which do not have a human ortholog but have a GENE_NAME in the species of origin will have their names retained.
    host
        The version of the Ensembl database to query, based upon its archive url. For an up-to-date databse, uses "http://www.ensembl.org" by default.
    return_dict
        Whether or not to return a dictionary for subsequent renaming, rather than conducting the renaming in-place. Currently must be set to True.
    cutnonnamed
        Whether to exclude all genes without a human ortholog from the dictionary. Otherwise, they will be retained with new names GENE_STABLE ID (GENE_STABLE_ID).

    Returns
    -------
    dict
        A dictionary of {current gene name : new gene name}. If return_dict is false, the remapping occurs in-place on the AnnData object.
    """
    server = Server(host=host)
    dataset = (server.marts['ENSEMBL_MART_ENSEMBL'].datasets[dataset])
    myorthos = dataset.query(attributes=['ensembl_gene_id', 'hsapiens_homolog_associated_gene_name', 'hsapiens_homolog_ensembl_gene', 'external_gene_name', 'hsapiens_homolog_orthology_type'])
    mdgenenamel = dataset.query(attributes=['ensembl_gene_id', 'external_gene_name'])

    # Rename to Human gene name + opossum gene ID

    # make 1:many paralogs (duplicates in Md only) still map to human names
    myorthos1many = myorthos[(myorthos["Human homology type"] == "ortholog_one2many")] # only 1:many
    # count the iterations to find many Md:1 Hs
    counts = myorthos1many['Gene stable ID'].value_counts()
    counts = counts[counts == 1]
    # create list of only 1 Hs:many Md
    myorthos1many = myorthos1many[myorthos1many["Gene stable ID"].isin(counts.index)]
    # possible bug/function: if 1 paralog in Hs is annotated and 1 is not, will be retained manyHs:1Md relation
    # if ran in 2nd block, get 2698 mappings, if ran in last, get 2429
    # if uncomment 1st line, will rename 1:many with Md version, e.g. ND1 instead of MT-ND1 for ENSMODG00000021562
    #myorthos1many = myorthos1many[pd.isnull(myorthos1many)["Gene name"] == True] # not unknown Md name
    myorthos1many = myorthos1many[pd.isnull(myorthos1many)["Human gene name"] == False] # not unknown Human name

    # only 1:1 orthos
    myorthos11 = myorthos[myorthos["Human homology type"] == "ortholog_one2one"] # only 1 to 1
    myorthos11 = myorthos11.set_index("Gene stable ID") # make ID to index
    myorthos1many = myorthos1many.set_index("Gene stable ID") # make ID to index
    mdgenenameli = mdgenenamel.set_index("Gene stable ID") # make ID to index for full list

    # Run the renaming pattern

    newnames_dict, newnicks_dict = {}, {}
    for itemi in mdgenenamel['Gene stable ID']: # for all md genes according to BioMart
        mdgeneid = itemi
        if mdgeneid in myorthos11.index and pd.isna(myorthos11.loc[mdgeneid]["Human gene name"]) == False: #1:1
            hsgenename = myorthos11.loc[mdgeneid]["Human gene name"] # 1:1
        elif mdgeneid in myorthos1many.index and only11 == False: # 1:many
            hsgenename = myorthos1many.loc[mdgeneid]["Human gene name"] # 1:many
        elif pd.isna(mdgenenameli.loc[mdgeneid]["Gene name"]) == False and onlyhs == False: # opossum name
            hsgenename = mdgenenameli.loc[mdgeneid]["Gene name"] # opossum name
        else:
            if cutnonnamed == False:
                hsgenename = mdgeneid
            else:
                continue
        newnames_dict[itemi] = (hsgenename + " (" + mdgeneid + ")")
        newnicks_dict[itemi] = (hsgenename)

    # make dict out of new names for future reference
    
    if return_dict == True:
        if longnames == True:
            return(newnames_dict)
        else:
            return(newnicks_dict)
    #elif input_dat != None:
