from anndata import AnnData
from typing import Optional, Union
import pandas as pd

def search(adata : AnnData,
    search_term : str,
    key : str = None,
    mode : Union["exact","startswith","search"] = "search",
          ) -> str:
    """\
    Search for a gene in the `.var_names` slot of an AnnData object by a portion of the string. Useful, for instance, if complex gene names are being used, such as the "GENE_NAME (GENE_ID)" format used by scprep. If none is found, return an error.

    Parameters
    ----------
    adata
        Annotated data matrix.
    search_term
        The string to look for in `adata.var`.
    key
        The column in `adata.var` to search in, if not `adata.var_names`.
    mode
        In "starstwith" mode, a gene is returned if it starts with the `search_term`. In "search" mode, search anywhere in the gene name. In "exact" mode, the search_term must match the entry in `adata.var` exactly.

    Returns
    -------
    str
        The name of the gene in `adata.var_names`, if a single result is obtained. Otherwise returns an error listing possible hits.
    """
    if key is None:
        myseries = pd.Series(adata.var_names, index = adata.var_names)
    else:
        if key in adata.var.columns:
            myseries = adata.var[key]
        else:
            raise ValueError(key + " not in adata.var.columns!")
    if mode == "exact":
        answer = [match for match in myseries.items() if match[1] == search_term]
        if len(answer) == 0:
            raise ValueError(search_term + " Not Found! Try not using exact mode.")
    elif mode == "startswith":
        answer = [match for match in myseries.items() if match[1].startswith(search_term)]
    else:
        answer = [match for match in myseries.items() if search_term in match[1]]
    if len(answer) == 1:
        return(answer[0][0])
    elif len(answer) == 0:
        raise ValueError(search_term + " Not Found!")
    else:
        raise ValueError("Inprecise, choose one: " + str(answer))
