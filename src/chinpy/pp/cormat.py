import anndata
import pandas as pd
from .grouped_obs_mean import grouped_obs_mean
from typing import Optional, Union

def cormat(adata1 : anndata.AnnData,
    adata2 : anndata.AnnData,
    group_key1 : str,
    group_key2 : str,
    gene_symbols1 : Optional[str] = None,
    gene_symbols2 : Optional[str] = None,
    layer1 : Optional[str] = None,
    layer2 : Optional[str] = None,
    corr_method : Union[Union["pearson","spearman","kendall"],str] = "pearson",
    ) -> pd.DataFrame:
    """\
    Generate a cluster-wise correlation matrix between two annotated data objects.

    Parameters
    ----------
    adata1
        First annotated data matrix for comparison.
    adata2
        Second annotated data matrix for comparison.
    group_key1
        Column in `adata1.obs` to group observations by.
    group_key2
        Column in `adata2.obs` to group observations by.
    gene_symbols1
        Column in `adata1.var` to use to make variables comparable between samples 1 and 2, for instance gene IDs rather than names. Optional.
    gene_symbols2
        Column in `adata2.var` to use to make variables comparable between samples 1 and 2, for instance gene IDs rather than names. Optional.
    layer1
        Layer key of adata1 to use instead of `adata1.X`. Optional.
    layer2
        Layer key of adata2 to use instead of `adata2.X`. Optional.
    corr_method
        Method of correlation, from `pd.DataFrame.corr()`.

    Returns
    -------
    pd.DataFrame
        A matrix of correlation scores by identity label.
    """
    cordf1 = grouped_obs_mean(adata1, group_key=group_key1, gene_symbols=gene_symbols1, layer=layer1)
    cordf2 = grouped_obs_mean(adata2, group_key=group_key2, gene_symbols=gene_symbols2, layer=layer2)
    cordf_m = cordf1.join(cordf2, how="inner")
    cordf_m_corr = cordf_m.corr(corr_method)
    cordf_m_corr.drop(index = cordf2.columns,
                 columns = cordf1.columns, inplace=True)
    #import seaborn as sns
    #sns.heatmap(cordf_m_corr)
    return(cordf_m_corr)
