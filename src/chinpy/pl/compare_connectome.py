import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
from typing import Optional, Union

def edge_heatmap(con : pd.DataFrame,
    ):
    """\
    Plot a heatmap of total cell-cell interactions between clusters, imitating a CellPhoneDB heatmap.

    Parameters
    ----------
    con
        The pd.DataFrame containing the connectome. This should be filtered.

    Returns
    -------
    MatPlotLib object
    """
    

def differential_connectome_heatmap(dcon : pd.DataFrame,
    score_col : Union["score_norm","weight_scale_lfc","weight_norm_lft"] ="score_norm",
    min_score : Optional[float] = None,
    max_score : Optional[float] = None,
    center : Optional[float] = None,
    cmap : Optional[str] = None,
    title : Optional[str] = None,
    figsize : tuple = (7,5),
    xtickrot : Optional[float] = None,
    ):
    """\
    Plot a differential cell-cell interactome as a heatmap of perturbation scores of each receptor-ligand mechanism.

    Parameters
    ----------
    dcon
        The pd.DataFrame containing the differential connectome, produced by the `differential_connectome` function.

    Returns
    -------
    pd.DataFrame
        A cell-cell interactome containing only the differential interactions between the reference and test states.
    """
    if min_score is not None:
        dcon = dcon[dcon[score_col] > min_score]
    if max_score is not None:
        dcon = dcon[dcon[score_col] < max_score]
    con_plot = pd.DataFrame(dcon[score_col])
    con_plot.reset_index(inplace=True)
    con_plot["edge"] = con_plot["ligand"] + " - " + con_plot["receptor"]
    con_plot["vector"] = con_plot["source"] + " - " + con_plot["target"]
    con_plot.drop(["source","target","ligand","receptor"], axis=1, inplace=True)
    con_plot.set_index("edge", inplace=True)
    con_plot = pd.pivot(con_plot, columns=["vector"], values=score_col)
    # plot
    #with sns.axes_style("darkgrid"):
    fig, ax = plt.subplots(figsize=figsize)
    ax = sns.heatmap(con_plot, linewidths=.5, linecolor="lightgray", center=center, cmap=cmap, cbar_kws={"label":score_col})
    if title is not None:
        ax.set_title(title)
    ax.tick_params(color="gray", width=1)
    if xtickrot is not None:
        ax.set_xticklabels(ax.get_xticklabels(), rotation=xtickrot)
