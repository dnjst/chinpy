# seaborn plotting code based on https://seaborn.pydata.org/examples/pairgrid_dotplot.html by Michael Waskom.

import seaborn as sns
import pandas as pd
import numpy as np
from matplotlib.ticker import MultipleLocator

def compare_centrality(Gs,height=8, aspect=.3, pt_size=15):
    sns.set_theme(style="whitegrid")

    Gs_wide = pd.pivot(Gs, index="cells", columns="name")#.fillna(0)
    Gs_plot = Gs_wide["hub_score"].reset_index()

    # Make the PairGrid
    g = sns.PairGrid(Gs_plot,
                     x_vars=Gs_plot.columns[1:], y_vars=["cells"],
                     height=height, aspect=aspect)

    # Draw a dot plot using the stripplot function
    g.map(sns.stripplot, size=pt_size,
          #orient="h",
          jitter=False,
          #palette="flare_r",
          linewidth=1,
          edgecolor="w",
         )

    # Use the same x axis limits on all columns and add better labels
    g.set(xlim=(-.05, 1.05), xlabel="Hub Score", ylabel="")

    # Title the columns after the stages
    titles = Gs_plot.columns[1:]

    for ax, title in zip(g.axes.flat, titles):

        # Set ticks for axes, and proper titles
        ax.set(title=title, xticks=np.arange(0,1.1,0.1))
        ax.xaxis.set_major_locator(MultipleLocator(0.2))
        ax.xaxis.set_major_formatter('{x:.1f}')

        # For the minor ticks, use no labels; default NullFormatter, but display minor grid lines.
        ax.tick_params(which="major",bottom=True, color="lightgray")
        ax.grid(visible=True, axis="x", which="minor", linestyle="dashed")
        ax.grid(visible=True, axis="y", which="major", color="gray")
        ax.xaxis.set_minor_locator(MultipleLocator(0.1))

    sns.despine(left=True, bottom=True)
