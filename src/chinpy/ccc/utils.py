import pandas as pd

def convert_to_r(con):
    if type(con.index) != pd.core.indexes.numeric.Int64Index:
        con = con.reset_index()
    con.rename({"percent_source":"percent.source",
               "percent_target":"percent.target",
               "ligand_expression":"ligand.expression",
               "recept_expression":"recept.expression",
               "ligand_scale":"ligand.scale",
               "recept_scale":"recept.scale",
               "p_val_adj_lig":"p_val_adj.lig",
               "p_val_adj_rec":"p_val_adj.rec",
               "source_ligand":"source.ligand",
               "receptor_target":"receptor.target"}, axis=1, inplace=True)
    cols_to_order = ['source','target','ligand','receptor','pair','mode']
    new_columns = cols_to_order + (con.columns.drop(cols_to_order).tolist())
    con = con[new_columns]
    return(con)

def which_coords(df, match):
    result = df.isin([match])
    result_any = result.any()
    win_list = []
    for col in list(result_any[result_any == True].index):
        rows = list(result[col][result[col] == True].index) 
        for row in rows:
            win_list.append((row, col))
    return(win_list)

def foldchange(num, denom):
    if num >= denom:
        return(num / denom)
    else:
        return(-denom / num)
