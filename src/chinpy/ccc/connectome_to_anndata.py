from anndata import AnnData
import pandas as pd
import numpy as np
from typing import Optional, Union

def connectome_to_anndata(con : pd.DataFrame,
    source_key : str = "source",
    target_key : str = "target",
    ligand_key : str = "ligand",
    receptor_key : str = "receptor",
    metavar_cols : Optional[list] = ["mode"],
    metaobs_cols : Optional[list] = None,
    weight_key : str = "weight_norm",
    flatten_index : bool = True,
    remove_na : bool = True,
    ) -> AnnData:
    """\
    Convert a Connectome into an annotated data matrix for dimensional reduction and other manipulations using standard single-cell data science tools.

    Parameters
    ----------
    con
        A pandas.DataFrame containing a connectome.
    source_key
        Name of the column in which the source cell is stored.
    target_key
        Name of the column in which the target cell is stored.
    ligand_key
        Name of the column in which the ligand gene name is stored.
    receptor_key
        Name of the column in which the receptor gene name is stored.
    metaobs_cols
        Additional metadata columns in the connectome dataframe to preserve in the object. Make sure they unambiguously map to source-target pairs!
    metavar_cols
        Additional metadata columns in the connectome dataframe to preserve in the object. Make sure they unambiguously map to receptor-ligand pairs!
    weight_key
        Name of the column in which the weight score to be used is stored.
    flatten_index
        Whether flatten to "Cell Type 1 - Cell Type 2" and "Gene 1 - Gene 2" nomenclature, rather than keeping the MultiIndex dataframe. Leave as True in most cases.
    remove_na
        Whether to remove edges containing 'NA' (no mapping to original object - only useful if investigating orphan ligands and receptors).

    Returns
    -------
    anndata.AnnData
        An annotated data matrix with observations corresponding to cluster-cluster pairs and variables corresponding to receptor-ligand pairs.
    """
    if metavar_cols is None:
        metavar_cols = []
    if metaobs_cols is None:
        metaobs_cols = []
    # test if all columns present
    need_cols = [source_key, target_key, ligand_key, receptor_key] + metavar_cols + metaobs_cols
    for i in need_cols:
        if i not in con.columns:
            raise ValueError(i + " is not a valid column in the provided dataframe!")

    mydf = con.pivot(index=[source_key,target_key] + metaobs_cols, columns=[ligand_key,receptor_key] + metavar_cols,values=weight_key)
    if remove_na is True:
        mydf.dropna(axis = 1, inplace=True)
    vdata = AnnData(X = mydf, dtype=np.float32, obs=mydf.index.to_frame(), var=mydf.columns.to_frame())
    if flatten_index is True:
        vdata.obs_names = vdata.obs[source_key].map(str) + " - " + vdata.obs[target_key].map(str)
        vdata.var_names = vdata.var[ligand_key].map(str) + " - " + vdata.var[receptor_key].map(str)
    return(vdata.copy())
