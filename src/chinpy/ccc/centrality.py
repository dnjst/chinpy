from .filter_connectome import filter_connectome
from .connectome_to_igraph import connectome_to_igraph

import pandas as pd
import seaborn as sns
import numpy as np
from matplotlib.ticker import MultipleLocator
from tqdm import tqdm
from typing import Optional, Union

def centrality_plot_stack(Gs,height=20, aspect=.5, pt_size=12, legend_title="name", jitter=True):
    sns.set_theme(style="whitegrid")

    #Gs_wide = pd.pivot(Gs, index="cells", columns="name")#.fillna(0)
    #Gs_plot = Gs_wide[plot_score].reset_index()

    # Make the PairGrid
    g = sns.PairGrid(Gs, x_vars=["hub_score","auth_score"], y_vars=["mode"], hue="cells",
                     height=height, aspect=aspect)

    # Draw a dot plot using the stripplot function
    g.map(sns.stripplot, size=pt_size,
          #orient="h",
          #hue="name",
          jitter=jitter,
          #palette="flare_r",
          linewidth=1,
          edgecolor="w",
         )
    
    g.add_legend(title=legend_title)

    # Use the same x axis limits on all columns and add better labels
    g.set(xlim=(-.05, 1.05), ylabel="")

    # Title the columns after the stages
    titles = ["Outgoing","Incoming"]

    for ax, title in zip(g.axes.flat, titles):

        # Set ticks for axes, and proper titles
        ax.set(title=title, xticks=np.arange(0,1.1,0.1))
        ax.title.set_size(20)
        ax.xaxis.set_major_locator(MultipleLocator(0.2))
        ax.xaxis.set_major_formatter('{x:.1f}')

        # For the minor ticks, use no labels; default NullFormatter, but display minor grid lines.
        ax.tick_params(which="major",bottom=True, color="lightgray")
        ax.grid(visible=True, axis="x", which="minor", linestyle="dashed")
        ax.grid(visible=True, axis="y", which="major", color="gray", linewidth=2)
        ax.xaxis.set_minor_locator(MultipleLocator(0.1))

    sns.despine(left=True, bottom=True)

def compute_centrality(connectome, weight_attribute="weight_norm", group_by = "mode", groups_use = None, normalize=True, verbose=True, plot=True, height=8, aspect=.3, pt_size=15, plot_score="hub_score", **kwargs):

    # warn about scaled weight
    if weight_attribute == "weight_sc" and min_z == None:
        print("Weight attribute is 'weight_sc', recommend also setting min.z = 0 to avoid negative ligand and receptor scores")
    
    # filter connectomes
    con_filt = filter_connectome(connectome, remove_na = True, verbose=verbose, **kwargs)
    
    # get uniques
    if groups_use is None:
        groups = set(con_filt[group_by])
    else:
        groups = groups_use
    nn_total = {}
    for groupi in tqdm(groups):
        temp = con_filt[con_filt[group_by] == groupi]
        G = connectome_to_igraph(temp, weight_attribute=weight_attribute, verbose=False)
        hub = pd.DataFrame(G.hub_score(weights=weight_attribute, scale=True), index=G.vs["name"], columns=["hub_score"])
        auth = pd.DataFrame(G.authority_score(weights=weight_attribute, scale=True), index=G.vs["name"], columns=["auth_score"])
        wt_source = temp[["source",weight_attribute]].groupby(["source"]).sum().rename(columns=lambda x : x + "_source")
        wt_sink = temp[["target",weight_attribute]].groupby(["target"]).sum().rename(columns=lambda x : x + "_target")
        if normalize == True:
            total_edgeweight = sum(temp[weight_attribute])
            wt_source /= total_edgeweight
            wt_sink /= total_edgeweight
        nn_total[groupi] = pd.concat([hub, auth, wt_source, wt_sink], axis=1).rename_axis("cells").reset_index()
        nn_total[groupi][group_by] = groupi
        #if verbose is True:
            #print("Progress: {0} out of {1}".format(len(nn_total), len(groups)))
    #return(nn_total)
        
    # merge all data frames
    nn_flat = pd.concat(nn_total, axis=0).reset_index(drop=True)
    if plot is False:
        return(nn_flat)
    else:
        centrality_plot_stack(nn_flat, height=height, aspect=aspect, pt_size=pt_size)

def compare_centrality_plot(Gs,height=8, aspect=.3, pt_size=15, plot_score="hub_score"):
    sns.set_theme(style="whitegrid")

    Gs_wide = pd.pivot(Gs, index="cells", columns="name")#.fillna(0)
    Gs_plot = Gs_wide[plot_score].reset_index()

    # Make the PairGrid
    g = sns.PairGrid(Gs_plot,
                     x_vars=Gs_plot.columns[1:], y_vars=["cells"],
                     height=height, aspect=aspect)

    # Draw a dot plot using the stripplot function
    g.map(sns.stripplot, size=pt_size,
          #orient="h",
          jitter=False,
          #palette="flare_r",
          linewidth=1,
          edgecolor="w",
         )

    # Use the same x axis limits on all columns and add better labels
    score_tidy = plot_score.split("_")[0].title() + " Score"
    g.set(xlim=(-.05, 1.05), xlabel=score_tidy, ylabel="")

    # Title the columns after the stages
    titles = Gs_plot.columns[1:]

    for ax, title in zip(g.axes.flat, titles):

        # Set ticks for axes, and proper titles
        ax.set(title=title, xticks=np.arange(0,1.1,0.1))
        ax.title.set_size(20)
        ax.xaxis.set_major_locator(MultipleLocator(0.2))
        ax.xaxis.set_major_formatter('{x:.1f}')

        # For the minor ticks, use no labels; default NullFormatter, but display minor grid lines.
        ax.tick_params(which="major",bottom=True, color="lightgray")
        ax.grid(visible=True, axis="x", which="minor", linestyle="dashed")
        ax.grid(visible=True, axis="y", which="major", color="gray", linewidth=2)
        ax.xaxis.set_minor_locator(MultipleLocator(0.1))

    sns.despine(left=True, bottom=True)

def compare_centrality_plot_stack(Gs,height=8, aspect=.5, pt_size=15, legend_title="name", jitter=True):
    sns.set_theme(style="whitegrid")

    #Gs_wide = pd.pivot(Gs, index="cells", columns="name")#.fillna(0)
    #Gs_plot = Gs_wide[plot_score].reset_index()

    # Make the PairGrid
    g = sns.PairGrid(Gs, x_vars=["hub_score","auth_score"], y_vars=["cells"], hue="name",
                     height=height, aspect=aspect)

    # Draw a dot plot using the stripplot function
    g.map(sns.stripplot, size=pt_size,
          #orient="h",
          #hue="name",
          jitter=jitter,
          #palette="flare_r",
          linewidth=1,
          edgecolor="w",
         )
    
    g.add_legend(title=legend_title)

    # Use the same x axis limits on all columns and add better labels
    g.set(xlim=(-.05, 1.05), ylabel="")

    # Title the columns after the stages
    titles = ["Outgoing","Incoming"]

    for ax, title in zip(g.axes.flat, titles):

        # Set ticks for axes, and proper titles
        ax.set(title=title, xticks=np.arange(0,1.1,0.1))
        ax.title.set_size(20)
        ax.xaxis.set_major_locator(MultipleLocator(0.2))
        ax.xaxis.set_major_formatter('{x:.1f}')

        # For the minor ticks, use no labels; default NullFormatter, but display minor grid lines.
        ax.tick_params(which="major",bottom=True, color="lightgray")
        ax.grid(visible=True, axis="x", which="minor", linestyle="dashed")
        ax.grid(visible=True, axis="y", which="major", color="gray", linewidth=2)
        ax.xaxis.set_minor_locator(MultipleLocator(0.1))

    sns.despine(left=True, bottom=True)

def compare_centrality(connectome_dict : dict,
    weight_attribute : Union["weight_norm","weight_sc"] = "weight_norm",
    normalize : bool = True,
    plot : Optional[Union["split","stack"]] = "split",
    height : float = 8,
    aspect : float = .3,
    pt_size : float = 15,
    min_z : Optional[float] = None,
    plot_score : Union["hub_score","auth_score"] = "hub_score",
    legend_title : str = "name",
    jitter : bool = True,
    verbose : bool = True,
    **kwargs):
    """\
    Compare centrality measures (Kleinberg hub and authority scores) between multiple connectome dataframes stored in a dictionary.

    Parameters
    ----------
    connectome_dict
        A dictionary where all values are connectome dataframes.
    weight_attribute
        Which scores to use as edge weights. Accepts `"weight_sc"` or `"weight_norm"`.
    normalize
        Whether or not to normalize the centrality scores to 1.
    plot
        Which kind of plot to produce, if at all. Accepts `"split"`, `"stack"`, or None.
    height
        Height of the plot.
    aspect
        Aspect ratio of the plot.
    pt_size
        Point size of the plot.
    min_z
        Minimum z-score when filtering the connectomes.
    plot_score
        Which scores to plot. Accepts `"hub_score"` or `"auth_score"`.
    legend_title
        Title of the plot.
    jitter
        Whether to jitter the points in the plot.
    verbose
        Whether to print progress updates.

    Returns
    -------
    pandas.DataFrame
        A DataFrame containing the calculated centrality measures for plotting, returned if `plot` is not set to a valid option.
    """
    # warn about scaled weight
    if weight_attribute == "weight_sc" and min_z == None:
        print("Weight attribute is 'weight_sc', recommend also setting min.z = 0 to avoid negative ligand and receptor scores")
    
    # filter connectomes
    con_filt = {}
    for stage, con in tqdm(connectome_dict.items()):
        con_filt[stage] = filter_connectome(con, remove_na = True, verbose=verbose, min_z = min_z, **kwargs)
    
    # convert to graph
    nn_total = {}
    for stage, con in tqdm(con_filt.items()):
        G = connectome_to_igraph(con, weight_attribute=weight_attribute, verbose=verbose)
        hub = pd.DataFrame(G.hub_score(weights=weight_attribute, scale=True), index=G.vs["name"], columns=["hub_score"])
        auth = pd.DataFrame(G.authority_score(weights=weight_attribute, scale=True), index=G.vs["name"], columns=["auth_score"])
        #cells = set(con["source"]) | set(con["target"])
        wt_source = con[["source",weight_attribute]].groupby(["source"]).sum().rename(columns=lambda x : x + "_source")
        wt_sink = con[["target",weight_attribute]].groupby(["target"]).sum().rename(columns=lambda x : x + "_target")
        if normalize == True:
            total_edgeweight = sum(con[weight_attribute])
            wt_source /= total_edgeweight
            wt_sink /= total_edgeweight
        nn_total[stage] = pd.concat([hub, auth, wt_source, wt_sink], axis=1).rename_axis("cells").reset_index()
        nn_total[stage]["name"] = stage
        if verbose == True:
            print("Centrality calculations on Connectome {0} complete.".format(stage))
        
    # merge all data frames
    nn_flat = pd.concat(nn_total, axis=0).reset_index(drop=True)
    if plot == "stack":
        compare_centrality_plot_stack(nn_flat, height=height, aspect=aspect, pt_size=pt_size, legend_title=legend_title, jitter=jitter)
    elif plot == "split":
        compare_centrality_plot(nn_flat, height=height, aspect=aspect, pt_size=pt_size, plot_score=plot_score)
    else:
        return(nn_flat)
