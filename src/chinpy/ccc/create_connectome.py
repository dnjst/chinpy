from anndata import AnnData
import scanpy as sc
import squidpy as sp
from scipy.sparse import csc_matrix, issparse
import numpy as np
import pandas as pd
import itertools
import scprep
import time
from typing import Optional, Union

def decomplexify(intercell,SOURCE="ligand",TARGET="receptor"):
    if (SOURCE in intercell.columns) & (TARGET in intercell.columns):
        intercell_li = intercell.loc[:,[SOURCE,TARGET]].drop_duplicates().copy()
        intercell_li.index = intercell_li[SOURCE] + "|" + intercell_li[TARGET]
        intercell_li.index.name = "interaction"
        src = intercell_li.pop(SOURCE).apply(lambda s: str(s).split("_")).explode()
        src.name = SOURCE
        tgt = intercell_li.pop(TARGET).apply(lambda s: str(s).split("_")).explode()
        tgt.name = TARGET
        intercell_li2 = pd.merge(intercell_li, src, how="left", left_index=True, right_index=True)
        intercell_li2 = pd.merge(intercell_li2, tgt, how="left", left_index=True, right_index=True)
        intercell_li2 = intercell_li2.reset_index().drop_duplicates()
        intercell_li2[SOURCE + "_complex"] = [i.split("|")[0] for i in intercell_li2["interaction"]]
        intercell_li2[TARGET + "_complex"] = [i.split("|")[1] for i in intercell_li2["interaction"]]
        return(intercell_li2)
    else:
        raise ValueError("Source and target strings not found in columns of the interaction list!")

def create_connectome(adata : AnnData,
    cluster_key : str,
    clusters : Optional[sp.gr._ligrec.Cluster_t] = None,
    complex_policy : Union["min","all"] = "min",
    input_scale : Union["log1p","counts","sqrtcpm"] = "log1p",
    raw_scale : Union["log1p","counts","sqrtcpm"] = "counts",
    use_raw : bool = False,
    layer : Optional[str] = None,
    log_base : Union["e", int] = "e",
    renormalize : bool = True,
    min_cells_per_ident : Optional[int] = None,
    weight_definition_norm : Union["product","sum","mean"] = "product",
    weight_definition_cpm : Union["product","sum","mean"] = "product",
    weight_definition_scale : Union["product","sum","mean"] = "mean",
    cpm_threshold : float = 30,
    pct_threshold : float = 0.1,
    cellchat_param : float = 0.5,
    cellchat_sizenorm : bool = False,
    p_values : bool = True,
    p_values_method : Union["logreg","t-test","wilcoxon","t-test_overestim_var"] = "wilcoxon",
    interactions : Optional[sp.gr._ligrec.Interaction_t] = None,
    interactions_lkey : str = "genesymbol_intercell_source",
    interactions_rkey : str = "genesymbol_intercell_target",
    interactions_meta : Optional[Union[list,str]] = None,
    indexed : bool = False,
    incl_metadata : bool = True,
    verbose : bool = True,
    ) -> pd.DataFrame:
    """\
    Calculate a Raredon-style connectome.

    Connectome is a long-format list of edges between pairwise vectors of cells.

    Parameters
    ----------
    adata
        Annotated data matrix.
    cluster_key
        The key in `adata.obs` containing the cluster annotations.
    clusters
        Iterable object containing all of the clusters to be considered for analysis. Can be specified either as a sequence of tuple or just a sequence of cluster names, in which case all combinations are considered.
    complex_policy
        Policy on how to handle complexes. Valid options are:
            - `"min"` - select gene with the minimum average expression.
            - `"all"` - select all possible combinations.
    input_scale
        Scale of expression data stored in `adata.X`. Options are
            - `"log1p"` - natural logarithm of counts-per-ten-thousand.
            - `"counts"` - raw counts.
            - `"sqrtcpm"` - square root counts-per-million.
    raw_scale
        Scale of expression data stored in `adata.raw.X`, if used. Options are
            - `"log1p"` - natural logarithm of counts-per-ten-thousand.
            - `"counts"` - raw counts.
            - `"sqrtcpm"` - square root counts-per-million.
    use_raw
        Whether or not `adata.raw` contains data to be used in the analysis.
    layer
        An optional layer in the anndata object which holds the data.
    log_base
        Base of the logarithm applied to normalized counts in `adata.X`. Default "e".
    renormalize
        Whether or not to normalize counts per million to 1e6, and log-counts-per-10000 to 1e4. If false, the program continues assuming they are already normalized.
    min_cells_per_ident
        Excludes all clusters with fewer cells than this number from the analysis.
    weight_definition_norm
        Method of edgeweight definition from normalized data slot.
            - `"sum"` - adds values from sending and receiving clusters.
            - `"mean"` averages values from sending and receiving clusters.
            - `"product"` - multiplies values from sending and receiving clusters.
    weight_definition_cpm
        Method of edgeweight definition from the counts-per-million data slot.
            - `"sum"` - adds values from sending and receiving clusters.
            - `"mean"` averages values from sending and receiving clusters.
            - `"product"` - multiplies values from sending and receiving clusters.
    weight_definition_scale
        Method of edgeweight definition from scaled data slot.
            - `"sum"` - adds values from sending and receiving clusters.
            - `"mean"` averages values from sending and receiving clusters.
            - `"product"` - multiplies values from sending and receiving clusters.
    cpm_threshold
        Counts-per-million threshold to use when calculating binary threshold scores (an interaction occurs if expression in both sending and receiving clusters exceeds this value).
    pct_threshold
        Percent nonzero expression threshold to use when calculating binary threshold scores (an interaction occurs if expression in both sending and receiving clusters exceeds this value).
    cellchat_param
        The Kh parameter in the Hill function step of the CellChat scoring algorithm. Default is 0.5.
    cellchat_sizenorm
        Whether or not to multiply CellChat scores by (size of cluster 1 * size of cluster 2 / dataset size ^ 2), as proposed in the manuscript
    p_values
        Runs a differential expression test to calculate adjusted p-value for ligand and receptor expression within the input object.
    p_values_method
        The kind of test to run if a p-value test is running. Passed to `scanpy.tl.rank_genes_groups`.
    interactions
        Interaction to test. The type can be one of:
            - pandas.DataFrame - must contain at least 2 columns named "source" and "target".
            - dict - dictionary with at least 2 keys named "source" and "target".
            - typing.Sequence - Either a sequence of str, in which case all combinations are produced, or a sequence of tuple of 2 str or a tuple of 2 sequences.
        If None, the interactions are extracted from omnipath. Protein complexes can be specified by delimiting the components with "_", such as "alpha_beta_gamma".
    interactions_lkey
        If `interactions` is a pandas.DataFrame, what the name of the "source" column is.
    interactions_rkey
        If `interactions` is a pandas.DataFrame, what the name of the "target" column is.
    interactions_meta
        If `interactions` is a pandas.DataFrame, what the name of the additional metadata columns to retain. Optional.
    indexed
        Whether or not to index the output connectome DataFrame by ["source","target","ligand","receptor"].
    incl_metadata
        Whether or not to include metadata columns which are combinations of other columns, e.g. "edge" being "GENE1 - GENE2".
    verbose
        Whether to print progress updates.

    Returns
    -------
    pd.DataFrame
        A Connectome dataframe with rows equal to the number of cell-cell pairs times the number of receptor-ligand pairs.
    """
    SOURCE = "ligand"
    TARGET = "receptor"
    MODE = "mode"
    # init
    t_0 = time.perf_counter()
    if log_base == "e":
        log_base_num = np.e
    else:
        log_base_num = log_base

    # process interactions list
    if interactions_meta is None:
        interactions_meta = []
    elif isinstance(interactions_meta, str):
        interactions_meta = [interactions_meta]
    keep_cols = [interactions_lkey, interactions_rkey] + interactions_meta
    interactions_proc = interactions.loc[:,keep_cols]
    interactions_proc.rename(columns = {interactions_lkey:SOURCE, interactions_rkey:TARGET}, inplace=True)
    # retain unadjusted
    interactions_proc[SOURCE + "_complex"] = interactions_proc[SOURCE].copy()
    interactions_proc[TARGET + "_complex"] = interactions_proc[TARGET].copy()
    # decomplexify
    interactions_clean = decomplexify(interactions_proc, SOURCE=SOURCE, TARGET=TARGET)
    if verbose is True:
        print("Protein complex handling expanded {0} receptor-ligand interactions into {1}.".format(interactions_proc.shape[0], interactions_clean.shape[0]))
    # reorder interactions_clean
    cols_to_move = [SOURCE, TARGET, SOURCE + "_complex", TARGET + "_complex"]
    interactions_clean = interactions_clean[cols_to_move + [col for col in interactions_clean.columns if col not in cols_to_move]]
        
    # create fake anndata
    if use_raw == True:
        adata_fake = AnnData(adata.raw.X, obs = adata.obs, var = adata.raw.var)
    elif layer is not None:
        adata_fake = AnnData(adata.layers[layer], obs = adata.obs, var = adata.var)
    else:
        adata_fake = AnnData(adata.X, obs = adata.obs, var = adata.var)
    # uppercase all gene names
    adata_fake.var_names = adata_fake.var_names.str.upper()
    
    # generate data frames from squidpy.gr._ligrec
    if input_scale == "log1p":
        if not issparse(adata_fake.X):
            adata_fake.layers["cpm"] = log_base_num ** adata_fake.X - 1
        else:
            adata_fake.layers["cpm"] = log_base_num ** adata_fake.X.toarray() - 1
        if renormalize is True:
            adata_fake.layers["cpm"] = scprep.normalize.library_size_normalize(adata_fake.layers["cpm"], rescale=1e6)
    elif input_scale == "counts":
        adata_fake.layers["counts"] = adata_fake.X
        adata_fake.layers["cpm"] = scprep.normalize.library_size_normalize(adata_fake.X, rescale=1e6)
        #adata_fake.X = 
        adata_fake.X = scprep.transform.log(scprep.normalize.library_size_normalize(adata_fake.X, rescale=1e4), pseudocount=1, base=log_base)
    elif input_scale == "sqrtcpm":
        if not issparse(adata_fake.X):
            adata_fake.X = np.square(adata_fake.X)
        else:
            adata_fake.X = np.square(adata_fake.X.toarray())
        adata_fake.layers["cpm"] = adata_fake.X
        if renormalize is True:
            adata_fake.X = scprep.normalize.library_size_normalize(adata_fake.X, rescale=1e4)
        sc.pp.log1p(adata_fake)
    adata_fake.layers["log1p_scale"] = adata_fake.X
    sc.pp.scale(adata_fake, layer="log1p_scale")
    # subset
    if verbose is True:
        t_1 = time.perf_counter()
        print("Scaled expression matrices in {0:0.4f} seconds.".format(t_1 - t_0))
    
    # Create averages and other relevant cluster-wise metrics, of only these GOI
    cluster_avgs = pd.DataFrame(adata_fake.X.toarray(), index=adata_fake.obs.index, columns=adata_fake.var.index).groupby(adata_fake.obs[cluster_key].astype("string").astype("category").values).apply(lambda c: ((log_base_num ** c - 1).mean())).T
    cluster_avgs = scprep.transform.log(cluster_avgs, pseudocount=1, base=log_base)
    cluster_avgs_scale = pd.DataFrame(adata_fake.layers["log1p_scale"], index=adata_fake.obs.index, columns=adata_fake.var.index).groupby(adata_fake.obs[cluster_key].astype("string").astype("category").values).mean().T
    cluster_cpms = pd.DataFrame(adata_fake.layers["cpm"], index=adata_fake.obs.index, columns=adata_fake.var.index).groupby(adata_fake.obs[cluster_key].astype("string").astype("category").values).mean().T
    cluster_pcts = pd.DataFrame(adata_fake.layers["cpm"], index=adata_fake.obs.index, columns=adata_fake.var.index).groupby(adata_fake.obs[cluster_key].astype("string").astype("category").values).apply(lambda c: ((c > 0).sum() / len(c))).T
    cluster_sizes = pd.DataFrame(adata_fake.layers["cpm"], index=adata_fake.obs.index, columns=adata_fake.var.index).groupby(adata_fake.obs[cluster_key].astype("string").astype("category").values).apply(lambda c: (len(c))).T

    # fill in zeros for missing genes
    all_genes = set(interactions_clean[SOURCE]).union(set(interactions_clean[TARGET]))
    def fill_missing_with_zeroes(avg_df, all_genes_df):
        missing_genes = [i for i in all_genes_df if i not in avg_df.index]
        zero_df = pd.DataFrame(np.zeros((len(missing_genes), len(avg_df.columns))), columns=avg_df.columns, index=missing_genes)
        df = pd.concat([avg_df, zero_df])
        return(df)
    cluster_avgs = fill_missing_with_zeroes(cluster_avgs, all_genes)
    cluster_avgs_scale = fill_missing_with_zeroes(cluster_avgs_scale, all_genes)
    cluster_cpms = fill_missing_with_zeroes(cluster_cpms, all_genes)
    cluster_pcts = fill_missing_with_zeroes(cluster_pcts, all_genes)
    
    dictdata = {"avgs":np.array(cluster_avgs),"avgs_scale":np.array(cluster_avgs_scale),
                "cpms":np.array(cluster_cpms),"pcts":np.array(cluster_pcts),
                "sizes":np.array(cluster_sizes)}

    if verbose is True:
        t_2 = time.perf_counter()
        print(f"Calculated cluster averages in {t_2 - t_1:0.4f} seconds.")

    interactions_tuples = [tuple(r) for r in interactions_clean.to_numpy()]
    #return(interactions_tuples)
    
    # Identify ligands and receptors expressed in the object
    gene_vectors = tuple(zip(interactions_clean[SOURCE], interactions_clean[TARGET]))
    #return(gene_vectors)
    genes_use = set(list(itertools.chain.from_iterable(gene_vectors)))
    
    # Convert to multiple-row-name format
    # REMOVED

    # clusters from squidpy.gr._ligrec
    # clusters are 2-cell type "cell_vectors", in Connectome's language
    if clusters is None:
        clusters = list(map(str, adata_fake.obs[cluster_key].cat.categories))
    if min_cells_per_ident is not None:
        len_pre = len(clusters)
        clusters_sized = dict(adata_fake.obs.groupby(cluster_key)[cluster_key].count())
        clusters = [i for i in clusters if clusters_sized[i] >= min_cells_per_ident]
        if verbose is True:
            print("Filtered based on cluster size: {0} of {1} retained.".format(len(clusters), len_pre))
    if all(isinstance(c, str) for c in clusters):
        clusters = list(itertools.product(clusters, repeat=2))  # type: ignore[no-redef,assignment]
    clusters = sorted(
            sp.gr._ligrec._check_tuple_needles(
                clusters,  # type: ignore[arg-type]
                adata.obs[cluster_key].cat.categories,
                msg="Invalid cluster `{0!r}`.",
                reraise=True,
            )
        )
    cluster_mapper = dict(zip(cluster_avgs.columns, range(len(cluster_avgs.columns))))
    gene_mapper = dict(zip(cluster_avgs.index, range(len(cluster_avgs.index))))
    clusters_flat = list({c for cs in clusters for c in cs})
    if verbose is True:
        t_3 = time.perf_counter()
        print(f"Finished processing clusters in {t_3 - t_2:0.4f} seconds.")
    
    # Generate full connectome
    if verbose is True:
        print("Creating connectome from {0} receptor-ligand interactions and {1} cell-cell vectors for a total length of {2}...".format(interactions_clean.shape[0], len(clusters), interactions_clean.shape[0] * len(clusters)))
    # relic from WIDE format of squidpy ->
    #res = pd.DataFrame(index=pd.MultiIndex.from_frame(interactions_clean, names=[SOURCE, TARGET]), columns=pd.MultiIndex.from_tuples(clusters, names=["cluster_1", "cluster_2"]),)
    
    # make a CELL VECTORS * GENE_VECTORS x SCORES matrix (LONG format)
    edges = [tuple(itertools.chain.from_iterable(i)) for i in list(itertools.product(clusters, interactions_tuples))]
    edges_df = pd.DataFrame(edges,
            columns=["source","target","ligand","receptor"] + list(interactions_clean.iloc[:,2:].columns))
    #return(gene_mapper)
    edges_ids = [(cluster_mapper[j[0]], cluster_mapper[j[1]], gene_mapper[j[2]], gene_mapper[j[3]]) for j in edges]
    #return(edges_ids)
    if verbose is True:
        t_4 = time.perf_counter()
        print(f"Constructed edgelist in {t_4 - t_3:0.4f} seconds.")
    
    con = pd.DataFrame(index=pd.MultiIndex.from_frame(edges_df))

    # simple calc columns
    con["size_source"] = [dictdata["sizes"][i[0]] for i in edges_ids]
    con["size_target"] = [dictdata["sizes"][i[1]] for i in edges_ids]
    con["percent_source"] = [dictdata["pcts"][i[2],i[0]] for i in edges_ids]
    con["percent_target"] = [dictdata["pcts"][i[3],i[1]] for i in edges_ids]
    con["ligand_expression"] = [dictdata["avgs"][i[2],i[0]] for i in edges_ids]
    con["recept_expression"] = [dictdata["avgs"][i[3],i[1]] for i in edges_ids]
    con["ligand_scale"] = [dictdata["avgs_scale"][i[2],i[0]] for i in edges_ids]
    con["recept_scale"] = [dictdata["avgs_scale"][i[3],i[1]] for i in edges_ids]
    con["ligand_cpm"] = [dictdata["cpms"][i[2],i[0]] for i in edges_ids]
    con["ligand_sqrtcpm"] = np.sqrt(con["ligand_cpm"])
    con["recept_cpm"] = [dictdata["cpms"][i[3],i[1]] for i in edges_ids]
    con["recept_sqrtcpm"] = np.sqrt(con["recept_cpm"])
    if verbose is True:
        t_5 = time.perf_counter()
        print(f"Looked up simple metadata in {t_5 - t_4:0.4f} seconds.")
        
    # connectome score columns
    if weight_definition_norm == "product":
        con["weight_norm"] = con.loc[:,['ligand_expression','recept_expression']].product(axis=1)
    elif weight_definition_norm == "sum":
        con["weight_norm"] = con.loc[:,['ligand_expression','recept_expression']].sum(axis=1)
    elif weight_definition_norm == "mean":
        con["weight_norm"] = con.loc[:,['ligand_expression','recept_expression']].mean(axis=1)

    if weight_definition_cpm == "product":
        con["weight_cpm"] = con.loc[:,['ligand_cpm','recept_cpm']].product(axis=1)
        con["weight_sqrtcpm"] = np.sqrt(con.loc[:,['ligand_cpm','recept_cpm']]).product(axis=1)
    elif weight_definition_cpm == "sum":
        con["weight_cpm"] = con.loc[:,['ligand_cpm','recept_cpm']].sum(axis=1)
        con["weight_sqrtcpm"] = np.sqrt(con.loc[:,['ligand_cpm','recept_cpm']]).sum(axis=1)
    elif weight_definition_cpm == "mean":
        con["weight_cpm"] = con.loc[:,['ligand_cpm','recept_cpm']].mean(axis=1)
        con["weight_sqrtcpm"] = np.sqrt(con.loc[:,['ligand_cpm','recept_cpm']]).mean(axis=1)

    if weight_definition_scale == "product":
        con["weight_sc"] = con.loc[:,['ligand_scale','recept_scale']].product(axis=1)
    elif weight_definition_scale == "sum":
        con["weight_sc"] = con.loc[:,['ligand_scale','recept_scale']].sum(axis=1)
    elif weight_definition_scale == "mean":
        con["weight_sc"] = con.loc[:,['ligand_scale','recept_scale']].mean(axis=1)

    # threshold scores
    con["cpm_threshold"] = (con.loc[:,['ligand_cpm','recept_cpm']] >= cpm_threshold).all(axis=1)
    con["pct_threshold"] = (con.loc[:,['percent_source','percent_target']] >= pct_threshold).all(axis=1)

    # CellChat score
    if cellchat_sizenorm is True:
        con["prob_cellchat"] = (con.loc[:,['ligand_expression','recept_expression']].product(axis=1) / (con.loc[:,['ligand_expression','recept_expression']].product(axis=1) + cellchat_param)) * (con.loc[:,['size_source','size_target']].product(axis=1) / (adata_fake.shape[0] ** 2))
    else:
        con["prob_cellchat"] = (con.loc[:,['ligand_expression','recept_expression']].product(axis=1) / (con.loc[:,['ligand_expression','recept_expression']].product(axis=1) + cellchat_param))

    if verbose is True:
        t_6 = time.perf_counter()
        print(f"Calculated CCC scores in {t_6 - t_5:0.4f} seconds.")
        
    # p values for differential gene expression by cluster
    if p_values == True:
        sc.tl.rank_genes_groups(adata_fake, groupby=cluster_key, method=p_values_method)
        pval_result = adata_fake.uns['rank_genes_groups']
        pval_groups = pval_result['names'].dtype.names
        pval_dict = {}
        for group in pval_groups:
            pval_dict[group] = pd.Series(pval_result['pvals_adj'][group], index = pval_result['names'][group])
        pval_result_df = pd.DataFrame(pval_dict)
        con["p_val_adj_lig"] = [pval_result_df.loc[i[2],i[0]] for i in con.index]
        con["p_val_adj_rec"] = [pval_result_df.loc[i[3],i[1]] for i in con.index]
        if verbose is True:
            t_7 = time.perf_counter()
            print(f"Calculated p values in {t_7 - t_6:0.4f} seconds.")
        
    # bogus metadata columns
    if incl_metadata == True:
        con["pair"] = con.index.to_frame()['ligand'] + "|" + con.index.to_frame()['receptor']
        con["vector"] = con.index.to_frame()['source'] + "|" + con.index.to_frame()['target']
        con["source_ligand"] = con.index.to_frame()['source'] + "|" + con.index.to_frame()['ligand']
        con["receptor_target"] = con.index.to_frame()['receptor'] + "|" + con.index.to_frame()['target']
        con["edge"] = con["source_ligand"] + "|" + con["receptor_target"]

    # reset index
    con.reset_index(inplace = True)
    con.sort_values(["source","target","ligand_complex","ligand","receptor_complex","receptor"], inplace=True)
        
    # handle MIN or ALL mode
    if complex_policy == "min":
        con = con.sort_values(["source","target","interaction"] + ["weight_norm"]).drop_duplicates(["source","target","interaction"])

    # columns to index, if requested
    if indexed == True:
        con.set_index(["source","target","ligand_complex","receptor_complex"], inplace=True)
    # cleanup
    del adata_fake
    if verbose is True:
        t_end = time.perf_counter()
        print(f"Calculated connectome in a total {t_end - t_0:0.4f} seconds.")
    return(con)
