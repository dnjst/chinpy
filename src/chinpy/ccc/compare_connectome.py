import math
import pandas as pd
from typing import Optional, Union
from .utils import which_coords

def differential_connectome(con_ref : pd.DataFrame,
    con_test : pd.DataFrame,
    exclude_unmatched : bool = True,
    min_pct : float = 0,
    min_score : Optional[float] = None,
    weight_attribute : Union["weight_norm","weight_sc"] = "weight_norm",
    ) -> pd.DataFrame:
    """\
    Compare two Raredon-style Connectomes to retrieve differential interacitons.

    Parameters
    ----------
    con_ref
        A pd.DataFrame containing the reference Connectome.
    con_test
        A pd.DataFrame containing the Connectome to be considered the experimental condition.
    exclude_unmatched
        Whether to exclude all interactions between cells which do not appear in both states. If False, the programs fills missing spaces with zero expression.
    min_pct
        Optional thresholding to remove connections where the percent of cells expressing either the ligand or receptor does not exceed a certain threshold.
    min_score
        Optional thresholding to remove connections where the score does not exceed a certain threshold.
    weight_attribute
        The weight attribute to use for thresholding. Either "weight_norm" or "weight_sc".

    Returns
    -------
    pd.DataFrame
        A cell-cell interactome containing only the differential interactions between the reference and test states.
    """
    # sort connectomes
    con_ref = con_ref.sort_values(["source","target","ligand","receptor"], axis=0)
    con_test = con_test.sort_values(["source","target","ligand","receptor"], axis=0)
    # trim connectomes
    con_ref = con_ref.set_index(["source","target","ligand","receptor"]).drop(["pair","vector","source_ligand","receptor_target","edge"], axis=1)
    con_test = con_test.set_index(["source","target","ligand","receptor"]).drop(["pair","vector","source_ligand","receptor_target","edge"], axis=1)
    if exclude_unmatched is True:
        # shave connectomes
        con_intersect = [value for value in con_ref.index if value in con_test.index]
        con_ref = con_ref.loc[con_intersect]
        con_test = con_test.loc[con_intersect]
    else:
        # fill non-present cell types with 0's
        merged_index = con_ref.index.union(con_test.index)
        merged_columns = con_ref.columns.union(con_test.columns)
        modes_ext = con_ref["mode"].combine_first(con_test["mode"])
        con_ref = pd.DataFrame(con_ref, index=merged_index, columns=merged_columns).fillna(0)
        con_test = pd.DataFrame(con_test, index=merged_index, columns=merged_columns).fillna(0)
        con_ref["mode"] = modes_ext
        con_test["mode"] = modes_ext
    con_res = pd.DataFrame(con_ref["mode"])
    con_res["ligand_expression_ref"] = con_ref["ligand_expression"]
    con_res["recept_expression_ref"] = con_ref["recept_expression"]
    con_res["ligand_expression_test"] = con_test["ligand_expression"]
    con_res["recept_expression_test"] = con_test["recept_expression"]
    con_res["pct_source_ref"] = con_ref["percent_source"]
    con_res["pct_target_ref"] = con_ref["percent_target"]
    con_res["pct_source_test"] = con_test["percent_source"]
    con_res["pct_target_test"] = con_test["percent_target"]
    con_res["ligand_expression_lfc"] = con_res["ligand_expression_test"] - con_res["ligand_expression_ref"]
    con_res["recept_expression_lfc"] = con_res["recept_expression_test"] - con_res["recept_expression_ref"]
    con_res["weight_norm_lfc"] = con_test["weight_norm"] - con_ref["weight_norm"]
    con_res["weight_scale_lfc"] = con_test["weight_sc"] - con_ref["weight_sc"]
    con_res["score_norm"] = abs(con_res["ligand_expression_lfc"]) * abs(con_res["recept_expression_lfc"])
    # threshold
    if min_pct is not None:
        con_res = con_res[((con_res["pct_source_ref"] > min_pct) & (con_res["pct_target_ref"] > min_pct)) | ((con_res["pct_source_test"] > min_pct) & (con_res["pct_target_test"] > min_pct))]
    if min_score is not None:
        con_res = con_res[con_res["score_norm"] > min_score]
    return(con_res)

def differential_squidpy(ref, test, alpha=0.05, **kwargs):
    #res_intersect = [value for value in ref['pvalues'].columns if value in test['pvalues'].columns]
    #ref_intersect = ref['pvalues'].loc[:,res_intersect]
    #test_intersect = test['pvalues'].loc[res_intersect]
    ref_sig = which_coords(ref['pvalues'] < alpha, True)
    test_sig = which_coords(test['pvalues'] < alpha, True)
    test_uniq_sig = [value for value in test_sig if value not in ref_sig]
    leftover_df = pd.DataFrame([[i[0][0], i[0][1], i[1][0], i[1][1]] for i in test_uniq_sig], columns=["source","target","cluster_1","cluster_2"])
    leftover_pval = leftover_df
    leftover_means = leftover_df
    leftover_pval["pvalues"] = [test['pvalues'].loc[i] for i in test_uniq_sig]
    leftover_means["means"] = [test['means'].loc[i] for i in test_uniq_sig]
    leftover_pval = pd.pivot(leftover_pval, values="pvalues", columns=["cluster_1","cluster_2"], index=["source","target"])
    leftover_means = pd.pivot(leftover_means, values="means", columns=["cluster_1","cluster_2"], index=["source","target"])
    #leftover_pval = pd.Series([res["LG"]['pvalues'].loc[i] for i in test_uniq_sig], index=test_uniq_sig)
    leftover_dict = {'means':leftover_means,'pvalues':leftover_pval,'metadata':test['metadata']}
    leftover_dict["means"].sort_index(inplace=True)
    leftover_dict["pvalues"].sort_index(inplace=True)
    leftover_dict["metadata"].sort_index(inplace=True)
    return(leftover_dict)
