import pandas as pd
import omnipath as op
from typing import Optional, Union

def annotate_modes(interactions : pd.DataFrame,
    interactions_lkey : str = "genesymbol_intercell_source",
    interactions_rkey : str = "genesymbol_intercell_target",
    annotation_source : Union["Connectome","CellChatDB"] = "CellChatDB",
    uncat_string : str = "UNCAT",
    trim_columns: bool = True) -> pd.DataFrame:
    """\
    Annotate a `pandas.DataFrame` with "ligand" and "receptor" columns with a third column, corresponding to an ontology category grouping the interactions.

    For the meantime, this function draws lists from either CellChatDB or Connectome lists - future support for custom annotation methods is in the works.

    The function will annotate all matching interactions, and ones for which a match is not found will be labeled according to `uncat_string`.

    Parameters
    ----------
    interactions
        A pandas DataFrame full of receptor-ligand interactions.
    interactions_lkey
        The column name of the sources/ligands.
    interactions_rkey
        The column name of the targets/receptors.
    annotation_source
        From which source to draw interactions.
    trim_columns
        Whether or not to trim columns (currently unused).
    uncat_string
        What to classify interactions which fail to fall into a category as.

    Returns
    -------
    pd.DataFrame
        An annotated dataframe of receptor-ligand pairs.
    """
    SOURCE = "source"
    TARGET = "target"
    MODE = "mode"
    UNCAT = uncat_string
    if annotation_source == "Connectome":
        connectome_annot = pd.read_csv("https://gitlab.com/wandplabs/connectome-lists/-/raw/main/ncomms8866_human.csv")
        connectome_annot = connectome_annot.loc[:,["Ligand.ApprovedSymbol","Receptor.ApprovedSymbol","mode"]]
        annotations_dict_t = dict(zip(tuple(zip(connectome_annot["Ligand.ApprovedSymbol"],connectome_annot["Receptor.ApprovedSymbol"])), connectome_annot["mode"]))
    elif annotation_source == "CellChatDB":
        # download CellChatDB annotations as converted by OmniPath, convert to dict
        intercell_annot = op.requests.Annotations.get(resources = [annotation_source])
        annotations = pd.pivot(intercell_annot, index="record_id", columns=["label"],values=["genesymbol","value"])
        annotations = annotations[(annotations['value']['role'] == "ligand") | (annotations['value']['role'] == "receptor")]
        annotations = annotations[pd.isnull(annotations["value"]["pathway"]) == False]
        annotations_dict_lig = annotations[annotations["value"]["role"] == "ligand"].loc[:,[("genesymbol","category"),("value","pathway")]].set_index(("genesymbol","category"))[("value","pathway")].to_dict()
        annotations_dict_rec = annotations[annotations["value"]["role"] == "receptor"].loc[:,[("genesymbol","category"),("value","pathway")]].set_index(("genesymbol","category"))[("value","pathway")].to_dict()
    #return(annotations_dict_lig)

    # annotate the modes
    modelist = []
    interactions_new = interactions.loc[:,[interactions_lkey,interactions_rkey]].set_axis([SOURCE, TARGET], axis='columns').drop_duplicates()
    for index, row in interactions_new.iterrows():
        if row[SOURCE] in annotations_dict_lig.keys():
            mymode1 = annotations_dict_lig[row[SOURCE]]
        else:
            mymode1 = UNCAT
        if row[TARGET] in annotations_dict_rec.keys():
            mymode2 = annotations_dict_rec[row[TARGET]]
        else:
            mymode2 = UNCAT
        if mymode1 == mymode2:
            modelist.append(mymode1)
            continue
        elif mymode1 == UNCAT and mymode2 != UNCAT:
            modelist.append(mymode2)
            continue
        elif mymode2 == UNCAT and mymode1 != UNCAT:
            modelist.append(mymode1)
            continue
        else:
            # CONFLICT OCCURRED - NEED TO RESOLVE
            #print(row["source"], mymode1, row["target"], mymode2)
            modelist.append(mymode1)
    interactions_new[MODE] = modelist
    return(interactions_new)
