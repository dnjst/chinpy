from chinpy.ccc.create_connectome import create_connectome
from anndata import AnnData
import pandas as pd

def create_multistage_connectome(adata : AnnData,
    split_key : str,
    **kwargs) -> dict:
    """\
    A wrapper around `create_connectome` to split an annotated data matrix by a categorical metadata annotation (e.g., timepoint) and run on all categories independently. Useful because cells can only communicate with partners existing at the same time.

    Parameters
    ----------
    adata
        Annotated data matrix.
    split_key
        The key in `adata.obs` containing the categories to split data by.
    **kwargs
        Additional arguments to pass to `create_connectome`.

    Returns
    -------
    dict
        A dictionary of connectome dataframes with keys corresponding to unique categories of the `split_key` option (e.g. timepoints).
    """
    con = {}
    for stage in adata.obs[split_key].cat.categories:
        con[stage] = create_connectome(adata[adata.obs[split_key] == stage], **kwargs)
    return(con)
