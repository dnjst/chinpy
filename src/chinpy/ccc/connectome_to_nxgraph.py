import networkx as nx
import pandas as pd
from typing import Optional, Union

def connectome_to_nxgraph(con : pd.DataFrame,
    weight_attribute : str = "weight_norm",
    verbose : bool =True,
    ) -> nx.Graph:
    """\
    Convert a Raredon-style Connectome into an nxgraph graph.

    Parameters
    ----------
    con
        A pandas.DataFrame containing a connectome.
    weight_attribute
        Which scores to use as edge weights. Accepts `"weight_sc"` or `"weight_norm"`.
    verbose
        Whether to print progress updates.

    Returns
    -------
    anndata.AnnData
        An annotated data matrix with observations corresponding to cluster-cluster pairs and variables corresponding to receptor-ligand pairs.
    """
    G = nx.MultiDiGraph()
    # trim connectome
    con_trim = con.loc[:,["source","target",weight_attribute]].to_numpy()
    con_tup = [tuple(r) for r in con_trim]
    G.add_nodes_from(list(set(con['source'])))
    # add edges
    G.add_weighted_edges_from(con_tup, weight=weight_attribute)
    #for i in con.values:
        #G.add_edge(i[0], i[1], pvalue=i[2], ligand=i[3], receptor=i[4], weight=i[5])
    if verbose == True:
        print("Converted connectome into nxgraph with {0} nodes and {1} edges.".format(G.number_of_nodes(), G.number_of_edges()))
    return(G)
