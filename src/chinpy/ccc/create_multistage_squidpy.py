import squidpy as sp
from anndata import AnnData
import pandas as pd

def create_multistage_squidpy(adata : AnnData,
    split_key : str,
    **kwargs) -> dict:
    """\
    A wrapper around `squidpy.gr.ligrec` to split an annotated data matrix by a categorical metadata annotation (e.g., timepoint) and run on all categories independently. Useful because cells can only communicate with partners existing at the same time.

    Parameters
    ----------
    adata
        Annotated data matrix.
    split_key
        The key in `adata.obs` containing the categories to split data by.
    **kwargs
        Additional arguments to pass to `squidpy.gr.ligrec`.

    Returns
    -------
    dict
        A dictionary of squidpy results dictionaries ("pvalues","means", and "metadata" dataframes) with keys corresponding to unique categories of the `split_key` option (e.g. timepoints).
    """
    con = {}
    for stage in adata.obs[split_key].cat.categories:
        con[stage] = sp.gr.ligrec(adata[adata.obs[split_key] == stage], **kwargs)
    return(con)
