import pandas as pd
from typing import Optional, Union

def filter_connectome(con : pd.DataFrame,
    min_pct : Optional[float] = None,
    max_p : Optional[float] = None,
    min_DOR : Optional[float] = None,
    min_exp : Optional[float] = None,
    min_cpm : Optional[float] = None,
    min_z : Optional[float] = None,
    modes_include : Optional[Union[set,list,tuple]] = None,
    modes_exclude : Optional[Union[set,list,tuple]] = None,
    sources_include : Optional[Union[set,list,tuple]] = None,
    targets_include : Optional[Union[set,list,tuple]] = None,
    mechanisms_include : Optional[Union[set,list,tuple]] = None,
    features : Optional[Union[set,list,tuple]] = None,
    verbose : bool = True,
    remove_na : bool = False,
    ) -> pd.DataFrame:
    """\
    Filter a Raredon-style connectome to remove edges not meeting given criteria.

    Parameters
    ----------
    con
        A pandas.DataFrame containing a connectome.
    min_pct
        Minimum fraction of cells within a given cluster expressing the ligand or receptor.
    max_p
        Maximum p-value for ligand and receptor. Filtration on this column requires prior p-value calculation.
    min_DOR
        Minimum log-normalized Diagnostic Odds Ratio for the ligand or receptor for its cell type within an edge. Currently not implemented.
    min_exp
        Minimum log-normalized expression level of ligand and receptor.
    min_cpm
        Minimum counts per million-normalized expression level of ligand and receptor.
    min_z
        Minimum z-score for ligand and receptor.
    modes_include
        Iterable of mode(s) to include.
    modes_exclude
        Iterable of mode(s) to exclude.
    sources_include
        Source nodes of interest. Output will be limited to edges coming from these sources.
    targets_include
        Target nodes of interest. Output will be limited to edges landing on these targets.
    mechanisms_include
        Ligand-Receptor pairs of interest. The input can be either an iterable of strings of Ligand and Receptor separated by " - " or an iterable of tuples.
    features
        An iterable of features to include as either ligands or receptors.
    verbose
        Whether to print progress updates.
    remove_na
        Whether to remove edges containing 'NA' (no mapping to original object - only useful if investigating orphan ligands and receptors).

    Returns
    -------
    pd.DataFrame
        A filtered Connectome dataframe with rows equal to the number of cell-cell pairs times the number of receptor-ligand pairs.
    """
    if verbose is True:
        print("Filtering connectome...")
    pre_filter = con.shape[0]
    
    if min_pct is not None:
        con = con[(con["percent_source"] > min_pct) & (con["percent_target"] > min_pct)]
    if min_exp is not None:
        con = con[(con["ligand_expression"] > min_exp) & (con["recept_expression"] > min_exp)]
    if min_DOR is not None:
        if "DOR_source" in con.columns and "DOR_target" in con.columns:
            con = con[(con["DOR_source"] > min_DOR) & (con["DOR_target"] > min_DOR)]
        elif verbose is True:
            print("DOR values not available; DOR filtration was not performed.")
    if min_z is not None:
        con = con[(con["ligand_scale"] > min_z) & (con["recept_scale"] > min_z)]
    if min_cpm is not None:
        con = con[(con["ligand_cpm"] > min_cpm) & (con["recept_cpm"] > min_cpm)]
    if max_p is not None:
        if "p_val_adj_lig" in con.columns and "p_val_adj_rec" in con.columns:
            con = con[(con["p_val_adj_lig"] < max_p) & (con["p_val_adj_rec"] > max_p)]
        elif verbose is True:
            print("p values not available; p-value filtration was not performed.")
    if modes_include is not None:
        con = con[con["mode"].isin(modes_include)]
    if modes_exclude is not None:
        con = con[~con["mode"].isin(modes_exclude)]
    if sources_include is not None:
        con = con[con["source"].isin(sources_include)]
    if targets_include is not None:
        con = con[con["target"].isin(targets_include)]
    if features is not None:
        con = con[(con["ligand"].isin(features)) | (con["receptor"].isin(features))]
    if mechanisms_include is not None:
        if " - " in mechanisms_include[0] and "pair" in con.columns:
            con = con[con["pair"].isin(mechanisms_include)]
        elif type(mechanisms_include[0]) is tuple:
            #return(con)
            con = con.iloc[[i in mechanisms_include for i in list(zip(con["ligand"], con["receptor"]))],:]
    if remove_na is True:
        con = con[~pd.isnull(con["percent_source"])]
        con = con[~pd.isnull(con["percent_target"])]
    
    post_filter = con.shape[0]
    if verbose is True:
        print("Pre-Filter edges: " + str(pre_filter))
        print("Post-Filter edges: " + str(post_filter))
        print("Connectome Filtration Complete!")
    return(con)
