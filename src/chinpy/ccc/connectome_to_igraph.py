from .filter_connectome import filter_connectome

import igraph
import pandas as pd
import seaborn as sns
from typing import Optional, Union

def connectome_to_igraph(con : pd.DataFrame,
    weight_attribute : Union["weight_norm","weight_sc"] = "weight_norm",
    verbose : bool = True,
    ) -> igraph.Graph:
    """\
    Convert a Raredon-style Connectome into an igraph graph.

    Parameters
    ----------
    con
        A pandas.DataFrame containing a connectome.
    weight_attribute
        Which scores to use as edge weights. Accepts `"weight_sc"` or `"weight_norm"`.
    verbose
        Whether to print progress updates.

    Returns
    -------
    anndata.AnnData
        An annotated data matrix with observations corresponding to cluster-cluster pairs and variables corresponding to receptor-ligand pairs.
    """
    # trim connectome
    #con_trim = con.loc[:,["source","target",weight_attribute]].to_numpy()
    #con_tup = [tuple(r) for r in con_trim]
    G = igraph.Graph.DataFrame(con, directed = True,)# edge_attrs = [weight_attribute])
    # add edges
    #G.add_weighted_edges_from(con_tup, weight=weight_attribute)
    #for i in con.values:
        #G.add_edge(i[0], i[1], pvalue=i[2], ligand=i[3], receptor=i[4], weight=i[5])
    if verbose == True:
        print("Converted connectome into igraph with {0} nodes and {1} edges.".format(len(G.vs), len(G.es)))
    return(G)

def igraph_plot(g : igraph.Graph,
    weight_attribute : Union["weight_norm","weight_sc"] = "weight_norm",
    palette="tab20",
    vertex_size : float = 60,
    vertex_label_size : float = 10,
    min_edge_width : float = 0,
    layout : Union["circle","kk"] = "circle",
    margin : float = 50,
    bbox_size : tuple = (800, 800),
    save_path : Optional[str] = None,
    verbose : bool =True,
):
    """\
    Plot a Connectome graph.

    Parameters
    ----------
    con
        A pandas.DataFrame containing a connectome.
    weight_attribute
        Which scores to use as edge weights. Accepts `"weight_sc"` or `"weight_norm"`.
    palette
        The listed colormap to use to color cell vertices, passed to `seaborn.color_palette`.
    vertex_size
        The size of vertices (cells).
    vertex_label_size
        The size of vertex labels.
    min_edge_width
        A constant value added to all of the edge widths.
    layout
        The short name of the type of graph. Accepts `"cirlce"`, `"kk"`, etc.
    margin
        The margin of white space on the outside to avoid labels and arrows being cut off.
    bbox_size
        The figure size.
    verbose
        Whether to print progress updates.

    Returns
    -------
    anndata.AnnData
        An annotated data matrix with observations corresponding to cluster-cluster pairs and variables corresponding to receptor-ligand pairs.
    """
    g.es["curved"] = True

    visual_style = {}

    # vertex options
    visual_style["vertex_size"] = vertex_size
    visual_style["frame_color"] = None
    color_dict = dict(zip(g.vs["name"], sns.color_palette(palette)))
    visual_style["vertex_color"] = [color_dict[gender] for gender in g.vs["name"]]
    visual_style["vertex_label"] = g.vs["name"]
    visual_style["vertex_label_size"] = vertex_label_size
    #visual_style["vertex_label_color"] = "gold"

    # edge options
    visual_style["edge_width"] = [i + min_edge_width for i in g.es[weight_attribute]]
    #visual_style["edge_label"] = g.es["receptor"]
    #visual_style["edge_label_size"] = 8

    # overall options
    visual_style["layout"] = g.layout(layout)
    visual_style["bbox"] = bbox_size
    visual_style["margin"] = margin
    if save_path is None:
        return(igraph.plot(g, **visual_style))
    else:
        igraph.plot(g, **visual_style, target=save_path)
