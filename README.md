## chinpy

Cell Interactomics Python Package

## Description
This project is a `python` re-implementation of various cell-cell signaling inference methods from single-cell transcriptomics data. It currently takes as input and can produce as output `anndata` objects.

## Visuals
See the `docs` folder.

![Example 2-d embedding of cell type interactome](https://gitlab.com/dnjst/chinpy/-/raw/main/docs/source/images/embedding-example.png)

## Installation
To install the latest version from pypi:

```
pip install -i https://test.pypi.org/simple/ chinpy --upgrade
```

To install from git:
```
pip install git+https://gitlab.com/dnjst/chinpy.git#egg=chinpy
```

## Documentation
Documentation is available at https://chinpy.readthedocs.io/en/latest/ (work in progress).

## Usage
How to construct a Connectome-style data frame from an `anndata.AnnData` object:

```
# load PBMC68k data set: adata.X scale is z-scored, adata.raw.X scale is log1p

adata = sc.datasets.pbmc68k_reduced()

# read in the omnipath database: can query the DB for CellPhoneDB/CellChatDB alone, or all DBs

intercell = op.interactions.import_intercell_network(
    include=['omnipath', 'pathwayextra', 'ligrecextra'], # define categories
    interactions_params={"databases": ["CellPhoneDB","CellChatDB"]}, # subset to just selected resources
)

# Remove backwards interactions (only allow ligand -> receptor), and filter by curation_effort

intercell_filtered = intercell[
    (intercell['category_intercell_source'] == 'ligand') & # set transmitters to be ligands
    (intercell['category_intercell_target'] == 'receptor') & # set receivers to be receptors
    (intercell['curation_effort'] >= 1) # must have at least 1 citation
]

# create connectome dataframe
con = create_connectome(adata,
    interactions=intercell_filtered,
    p_values=False,
    use_raw=True,
    raw_scale="log1p",
    cluster_key="bulk_labels",
    interactions_lkey="genesymbol_intercell_source",
    interactions_rkey="genesymbol_intercell_target",
    interactions_meta=["secreted_intercell_source","secreted_intercell_target"],
    complex_policy="all")

# save file
#con.to_csv("pbmc68k_reduced.csv")

# convert connectome to annotated data matrix
vdata = chinpy.ccc.connectome_to_anndata(con)
```

How to create multiple Connectome-style data frames from an `anndata.AnnData` object with multiple stages:

```

```

This will return a dictionary, with keys named after the different categorical entries in the `split_key` column of its metadata, and values corresponding to cell-cell connectomes for that stage.

How to create a ligand-receptor list:

```
SOURCE = "source"
TARGET = "target"
MODE="mode"
curation_effort = 1 # minimum number of citations needed

# read in the omnipath database: can query the DB for CellPhoneDB/CellChatDB alone, or all DBs
intercell = op.interactions.import_intercell_network(
    include=['omnipath', 'pathwayextra', 'ligrecextra'], # define categories
    interactions_params={"databases": ["CellPhoneDB","CellChatDB","Guide2Pharma_Cellinker","DLRP_Cellinker","CellPhoneDB_Cellinker","HPMR_Cellinker"]}, # subset to just selected resources
)

# filter to L-R interactions

intercell_filtered = intercell[
    (intercell['category_intercell_source'] == 'ligand') & # set transmitters to be ligands
    (intercell['category_intercell_target'] == 'receptor') & # set receivers to be receptors
    (intercell['curation_effort'] >= curation_effort) # must have at least 1 citation
]

# remove ANY interactions labeled as physical in ANY database
physical_interactions = ['adhesion', 'cell_adhesion', 'desmosome', 'ecm', 'ecm_regulator', 'gap_junction', 'tight_junction']
physical_list = []

for index, row in intercell.iterrows():
    if row['parent_intercell_target'] in physical_interactions or row['parent_intercell_source'] in physical_interactions:
        physical_list.append((row['genesymbol_intercell_source'], row['genesymbol_intercell_target']))
for index, row in intercell_filtered.iterrows():
    if (row['genesymbol_intercell_source'], row['genesymbol_intercell_target']) in set(physical_list):
        intercell_filtered = intercell_filtered.drop(index=index)
        
# isolate just interaction columns
intercell_filtered = intercell_filtered.loc[:,["genesymbol_intercell_source", "genesymbol_intercell_target"]]

# rename columns
intercell_filtered.columns = (SOURCE, TARGET)
```

See extended examples at https://gitlab.com/wandplabs/ligrec-enzymes.

How to annotate a ligand-receptor data frame:

```
intercell_annotated = chinpy.ccc.annotate_modes(intercell_filtered, annotation_source="CellChatDB",interactions_lkey="source",interactions_rkey="target")
intercell_annotated = intercell_annotated.sort_values(["source","target"])
```

## Support
The best location is https://gitlab.com/dnjst/chinpy/-/issues or e-mail to the authors.

## Roadmap
* Update `compare_connectome` to include multiple scoring methods.
* Add carry-over of cell-level metadata into the output data frames.
* Write vignette specifically for cell-cell vectors mode based upon `connectome_to_anndata` functionality.
* Add circos plotting through [pycircos](https://github.com/ponnhide/pyCircos).

## Contributing
Contributions welcome.

## Authors and acknowledgment
Thanks for early stage input from Micha Sam Raredon, from whose work some code is derived, per its license.

## License
GNU GPL v3.0

## Project status
Under active development, very unstable.

Currently, the `chinpy.ccc.create_connectome` implementation should be functional and produce `Connectome`-equivalent output dataframes. Visualization remains limited.
