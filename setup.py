import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="chinpy",
    version="0.0.55",
    author="Daniel Stadtmauer",
    author_email="daniel.stadtmauer@yale.edu",
    description="Software for studying cell-cell interactions.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/dnjst/chinpy",
    project_urls={
        "Bug Tracker": "https://gitlab.com/dnjst/chinpy/-/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.8",
)
