API
===============

This page contains the list of project's modules.

.. autopackagesummary:: chinpy
   :toctree: _autosummary
   :template: package.rst
