Installation
===============

The latest source code for the project is located at:

https://gitlab.com/dnjst/chinpy

It can be installed by running

```
pip install git+https://gitlab.com/dnjst/chinpy.git#egg=chinpy
```

The package is also located, for now, on the test pypi server, to be moved to the real one when it becomes mores stable. It can be installed by running:

```
pip install -i https://test.pypi.org/simple/ chinpy --upgrade
```

The pip method is recommended.
