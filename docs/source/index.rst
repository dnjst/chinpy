Welcome to documentation for the cell-cell communication software package!
==================================

This package takes single-cell gene expression data in the AnnData format (https://anndata.readthedocs.io/en/latest/) and outputs simple measures of inferred cell-cell communication in a standard format.

It is meant to support multiple scoring algorithms (see Vignettes).

The scores calculated currently include:

Law of Mass Action-based scores:

- *Log-Normalized Weight:* Product of log-TP10k (Raredon et al., 2022)
- *Scaled Weight:* Mean of z-scored expression (Raredon et al., 2022)
- *Square Root-Normalized Weight:* Product of square-root transcripts per million to avoid log multiplication
- *CellChat Probability:* Combined Hill function and law of mass action, optionally corrected for relative proportion of clusters (Jin et al., 2021)

Threshold-based scores:

- *Arbitrary TPM cutoff* (Pavlicev et al., 2017)

Statistical tests:

- *CellPhoneDB p-value:* currently only supported by external calling of `squidpy.gr.ligrec` (Palla et al., 2022) re-implementation of (Efremova et al., 2020).

==

The latest source code for the project is located at:

https://gitlab.com/dnjst/chinpy

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   installation
   api
   vignettes

Navigation
==================

* :ref:`genindex`
* :ref:`modindex`
