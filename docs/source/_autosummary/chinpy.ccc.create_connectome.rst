chinpy.ccc.create\_connectome
=============================

.. automodule:: chinpy.ccc.create_connectome
   :members:

   .. autopackagesummary:: chinpy.ccc.create_connectome
      :toctree: .
      :template: package.rst
