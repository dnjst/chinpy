chinpy.ccc.create\_multistage\_connectome
=========================================

.. automodule:: chinpy.ccc.create_multistage_connectome
   :members:

   .. autopackagesummary:: chinpy.ccc.create_multistage_connectome
      :toctree: .
      :template: package.rst
