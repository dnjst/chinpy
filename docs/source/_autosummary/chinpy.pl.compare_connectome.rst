chinpy.pl.compare\_connectome
=============================

.. automodule:: chinpy.pl.compare_connectome
   :members:

   .. autopackagesummary:: chinpy.pl.compare_connectome
      :toctree: .
      :template: package.rst
