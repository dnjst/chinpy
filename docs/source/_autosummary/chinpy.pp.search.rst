chinpy.pp.search
================

.. automodule:: chinpy.pp.search
   :members:

   .. autopackagesummary:: chinpy.pp.search
      :toctree: .
      :template: package.rst
