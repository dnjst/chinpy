chinpy.ccc.connectome\_to\_nxgraph
==================================

.. automodule:: chinpy.ccc.connectome_to_nxgraph
   :members:

   .. autopackagesummary:: chinpy.ccc.connectome_to_nxgraph
      :toctree: .
      :template: package.rst
