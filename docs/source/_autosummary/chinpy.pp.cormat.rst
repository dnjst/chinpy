chinpy.pp.cormat
================

.. automodule:: chinpy.pp.cormat
   :members:

   .. autopackagesummary:: chinpy.pp.cormat
      :toctree: .
      :template: package.rst
