﻿chinpy.pl
=========

.. automodule:: chinpy.pl
   :members:

   .. autopackagesummary:: chinpy.pl
      :toctree: .
      :template: package.rst
