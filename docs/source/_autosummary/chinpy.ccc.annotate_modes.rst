chinpy.ccc.annotate\_modes
==========================

.. automodule:: chinpy.ccc.annotate_modes
   :members:

   .. autopackagesummary:: chinpy.ccc.annotate_modes
      :toctree: .
      :template: package.rst
