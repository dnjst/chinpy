chinpy.ccc.compare\_connectome
==============================

.. automodule:: chinpy.ccc.compare_connectome
   :members:

   .. autopackagesummary:: chinpy.ccc.compare_connectome
      :toctree: .
      :template: package.rst
