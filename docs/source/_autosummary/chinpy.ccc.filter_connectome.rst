chinpy.ccc.filter\_connectome
=============================

.. automodule:: chinpy.ccc.filter_connectome
   :members:

   .. autopackagesummary:: chinpy.ccc.filter_connectome
      :toctree: .
      :template: package.rst
