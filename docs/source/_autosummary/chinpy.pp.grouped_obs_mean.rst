chinpy.pp.grouped\_obs\_mean
============================

.. automodule:: chinpy.pp.grouped_obs_mean
   :members:

   .. autopackagesummary:: chinpy.pp.grouped_obs_mean
      :toctree: .
      :template: package.rst
