chinpy.pp.remap
===============

.. automodule:: chinpy.pp.remap
   :members:

   .. autopackagesummary:: chinpy.pp.remap
      :toctree: .
      :template: package.rst
