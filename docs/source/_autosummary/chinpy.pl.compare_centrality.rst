chinpy.pl.compare\_centrality
=============================

.. automodule:: chinpy.pl.compare_centrality
   :members:

   .. autopackagesummary:: chinpy.pl.compare_centrality
      :toctree: .
      :template: package.rst
