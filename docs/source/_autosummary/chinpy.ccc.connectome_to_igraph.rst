chinpy.ccc.connectome\_to\_igraph
=================================

.. automodule:: chinpy.ccc.connectome_to_igraph
   :members:

   .. autopackagesummary:: chinpy.ccc.connectome_to_igraph
      :toctree: .
      :template: package.rst
